<?php

class testAPI{

	# If $env is set to "production", errors will be limited to messages only
	# If it is set to "dev" they will also show the file and line
	static $env='production';

	function __construct(){
		# In order to handle errors gracefully, we set custom function to take care of them
		set_error_handler(array('testAPI','ErrorHandler'),E_ALL|E_STRICT);

		# Make sure that the requested API method exists
		if(!method_exists($this,$_POST['method'])){trigger_error($_POST['method'].' is not a valid API method',E_USER_ERROR);}
		else{testAPI::$_POST['method']();}

	}

	# The function calls the VisitorStats class and obtains the data
	function GetMetrics($metrics='default'){

		# To fetch the data we need to make sure that the user has requested both "year" and "month"
		if(!isset($_POST['year'])){trigger_error("Please define 'year'",E_USER_ERROR);}
		if(!isset($_POST['month'])){trigger_error("Please define 'month'",E_USER_ERROR);}

		include('stats.php');
		$stats=new VisitorStats;

		$stats->getStats($_POST['year'],$_POST['month'],(isset($_POST['metrics'])?$_POST['metrics']:null));
	}

	# Function to handle the errors
	static public function ErrorHandler($errno,$errstr,$errfile=null,$errline=null){
		echo json_encode(array('error'=>true,"message"=>"Error: $errstr".(testAPI::$env=='dev'?"\nLine $errline : $errfile":'')));
		exit;
	}
}
new testAPI;