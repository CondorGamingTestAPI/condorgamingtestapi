<?php

class VisitorStats{

	# In case the user has not requested specific metrics, we use the metrics defined here
	protected $defaultMetrics=array('GoogleAnalytics','ApacheLogs');

	# This list only serves as "help" in case the user has requested a non-existent metric
	protected $availableMetrics='GoogleAnalytics, ApacheLogs, SpringMetrics, Woopra';

	static $statsData=array();

	# For the sake of the test, to keep things simple, data gets returned by the __destruct function
	function __destruct(){
		# The stats data gets rendered only if such was set
		if(!empty(VisitorStats::$statsData)){echo json_encode(array('error'=>false,'data'=>VisitorStats::$statsData));}
	}

	function getStats($year,$month,$metrics){
		# If the user has not requested specific metrics then we use the default metrics
		# If metrics are requested, they should be in CSV format, which is why we use str_getcsv to convert them into an array
		$metrics=($metrics==NULL?$this->defaultMetrics:str_getcsv($metrics));
		# If there are more than one metric requested, we go through them with foreach
		# but to avoid looping only single metric we check the array count
		if(count($metrics)==1){
			# We pass the metric fetching to a function, so it can validate the validity of the requested metric
			$this->getMetric($metrics[0]);
		}
		else{
			foreach($metrics as $metric){
				$this->getMetric($metric);
			}
		}
	}

	function getMetric($m){
		# To avoid calling undefined functions, we make sure that the requested metric exists
		if(method_exists($this,$m)){$this->$m($_POST['year'],$_POST['month']);}
		else{trigger_error("'".$m."' is not a defined metric. \nThe available metrics are: ".$this->availableMetrics,E_USER_ERROR);}
	}

	# With this function we check if the requested year and month exist in the data pool for the requested metric to prevent errors
	static function prepareData($data,$metric,$year,$month){
		if(!array_key_exists($year,$data)){
			VisitorStats::$statsData[$metric]="No data for year '".$year."'";
		}
		else{
			if(!array_key_exists($month,$data[$year])){
				VisitorStats::$statsData[$metric]="No data for month '".$month."', year '".$year."'";
			}
			else{
				VisitorStats::$statsData[$metric]=$data[$year][$month];
			}
		}
	}

	static function GoogleAnalytics($year,$month){
		/* That would be the function fetching the data from Google's servers, but in this case we will use dummy data */
		$dummy_stats=array('2016'=>array('1'=>'100','2'=>'120','3'=>'140','4'=>'160','5'=>'180','6'=>'200','7'=>'220','8'=>'240','9'=>'260','10'=>'280','11'=>'300','12'=>'320'),'2017'=>array('1'=>'340','2'=>'360'));

		VisitorStats::prepareData($dummy_stats,'GoogleAnalytics',$year,$month);
	}

	static function ApacheLogs($year,$month){
		/* Here we can create functionality for parsing data from the Apache logs */
		$dummy_stats=array('2016'=>array('1'=>'1000','2'=>'1200','3'=>'1400','4'=>'1600','5'=>'1800','6'=>'2000','7'=>'2200','8'=>'2400','9'=>'2600','10'=>'2800','11'=>'3000','12'=>'3200'),'2017'=>array('1'=>'3400','2'=>'3600'));

		VisitorStats::prepareData($dummy_stats,'ApacheLogs',$year,$month);
	}

	/* We can also use additional analytics tools, like SpringMetrics and Woopra */
	static function SpringMetrics($year,$month){
		$dummy_stats=array('2016'=>array('1'=>'200','2'=>'300','3'=>'400','4'=>'500','5'=>'600','6'=>'700','7'=>'800','8'=>'900','9'=>'1000','10'=>'1200','11'=>'1300','12'=>'1400'),'2017'=>array('1'=>'1500','2'=>'1600'));

		VisitorStats::prepareData($dummy_stats,'SpringMetrics',$year,$month);
	}
	static function Woopra($year,$month){
		$dummy_stats=array('2016'=>array('1'=>'300','2'=>'450','3'=>'600','4'=>'750','5'=>'900','6'=>'1050','7'=>'1200','8'=>'1350','9'=>'1500','10'=>'1650','11'=>'1800','12'=>'1950'),'2017'=>array('1'=>'2100','2'=>'2250'));

		VisitorStats::prepareData($dummy_stats,'Woopra',$year,$month);
	}

}