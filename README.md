# Demo of the API is available at

[http://dreal.eu/examples/condorGaming/](http://dreal.eu/examples/condorGaming/)


In order to test the API, fire up POST requests towards the URL. The parameters that you can use are::

 * method
 * year
 * month
 * metrics (optional)


The only method available in the test is "GetMetrics" since that's the only thing that the API is supposed to do as per the documentation.

#### Example payload

```javascript
method=GetMetrics&year=2016&month=12
```

#### Example Result
```json
{"error":false,"data":{"GoogleAnalytics":"320","ApacheLogs":"3200"}}
```